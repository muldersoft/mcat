% ![](img/mcat/mcat_logo.jpg)  
MCat
% by LoRd_MuldeR &lt;<mulder2@gmx>&gt; | <http://muldersoft.com/>


# Introduction #

**MCat** is a simple network port probing application for Windows, inspired by [*Netcat*](http://netcat.sourceforge.net/).


# Synopsis #

The *MCat* application is invoked as follows:
```
mcat.exe [OPTIONS] <target_name> [<port_number>]
```


# Parameters #

The *MCat* application takes the following parameters:

* **`target_name`** 
  Specifies the target host name. This can be a DNS name, an IPv4 address in "dot-decimal" notation, or an IPv6 address in hexadecimal "square brackets" notation. DNS names will be resolved automatically. *Required.*

* **`port_number`** 
  Specifies the target port number, in the 1 to 65535 range. If **no** port is explicitly specified, port 80 is used by default.


# Options

The *MCat* application supports the following options:

* **`--timeout <timeout_msec>`** 
  Set up the specified connection timeout, in milliseconds. If *not* specified, a system-specific default timeout applies.

* **`--retry <count>`** 
  Retry for the specified number of times, if connection attempt failed. By default, the application will *not* retry.

* **`--ipv4`** 
  Force the use of the IPv4 protocol. This will cause the application to fail with IPv6-only hosts. Default is *auto-detect*.

* **`--ipv6`** 
  Force the use of the IPv6 protocol. This will cause the application to fail with IPv4-only hosts. Default is *auto-detect*.

* **`--first-only`** 
  Only try to connect to the *first* IP address to which the host name resolved. Default will try *all* addresses.

* **`--silent`** 
  If this option is specified, the application will **not** output any progress/error messages to the standard error stream.

* **`--help`** 
  If this option is specified, the application will print the help screen to the console and then terminate immediately.


# Exit Codes #

The *MCat* application returns one of the following exit codes:

* **0**: Connection succeeded.

* **1**: Generic error, e.g. a required parameter was missing or an invalid/unsupported option was specified.

* **2**: The specified target name could not be resolved or the specified IP address is invalid.

* **3**: The connection failed. This could mean that the remote host refused to connection or that a timeout was triggered.

* **4**: System error, e.g. Winsock library failed to initialize.

* **5**: The application was interrupted. This is the expected result when the user aborts with Ctrl+C.


# Examples #

Here are some simple *MCat* usage examples:

* **Example #1:**
    ```
    mcat.exe www.example.com
    ```

* **Example #2:**
    ```
    mcat.exe www.example.com 443
    ```

* **Example #3:**
    ```
    mcat.exe --ipv4 --timeout 250 --retry 5 www.example.com 443
    ```


# License Terms #


The *MCat* application is released under the ***GNU General Public License, Version 2***.

	MCat - Simple Port Probing Tool
	Copyright (c) 2017 LoRd_MuldeR <mulder2@gmx.de>. Some rights reserved.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


<http://www.gnu.org/licenses/gpl-2.0.html>

## Acknowledgment ##

The "cat with long tail" ASCII art used in the logo was originally created by [*Joan Stark*](https://web-beta.archive.org/web/20130318224954/http://www.geocities.com/SoHo/7373/pets.htm).


# Source Code #

The source code of the *MCat* application is available from one of the official [**Git**](http://git-scm.com/) repository mirrors:
* `https://bitbucket.org/muldersoft/mcat.git` &nbsp; ([Browse](https://bitbucket.org/muldersoft/mcat))
* `https://gitlab.com/lord_mulder/MCat.git` &nbsp; ([Browse](https://gitlab.com/lord_mulder/MCat))


#Changelog

## Version 1.01 [2017-04-06]

* Correctly print Unicode strings to the console.

* Added new option `--silent` to disable all progress/error messages.

* Various fixes and improvements.

## Version 1.00 [2017-04-05]

* Initial release.


&nbsp;  
&nbsp;  

e.o.f.
