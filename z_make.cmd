@echo off
setlocal enabledelayedexpansion
cd /d "%~dp0"

REM ///////////////////////////////////////////////////////////////////////////
REM // Setup environment
REM ///////////////////////////////////////////////////////////////////////////

REM Setup paths
set "MSVC_PATH=C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC"
set "GIT2_PATH=C:\Program Files\Git\bin"

set "SOLUTION_POSTFIX=VC10"
set "PLATFORM_TOOLSET=v100"

REM ///////////////////////////////////////////////////////////////////////////
REM // Check paths
REM ///////////////////////////////////////////////////////////////////////////

if not exist "%MSVC_PATH%\vcvarsall.bat" (
	"%~dp0\..\Prerequisites\CEcho\cecho.exe" RED "\nMSVC not found.\n%MSVC_PATH:\=\\%\\vcvarsall.bat\n"
	pause & goto:eof
)

if not exist "%GIT2_PATH%\git.exe" (
	"%~dp0\..\Prerequisites\CEcho\cecho.exe" RED "\nGIT not found.\n%GIT2_PATH:\=\\%\\git.exe\n"
	pause & goto:eof
)

if not exist "%JAVA_HOME%\bin\java.exe" (
	"%~dp0\..\Prerequisites\CEcho\cecho.exe" "Java could not be found, please make sure JAVA_HOME is set correctly!"
	pause & goto:eof
)

REM ///////////////////////////////////////////////////////////////////////////
REM // Get current date and time (in ISO format)
REM ///////////////////////////////////////////////////////////////////////////

set "ISO_DATE="
set "ISO_TIME="
for /F "tokens=1,2 delims=:" %%a in ('"%~dp0\..\Prerequisites\GnuWin32\date.exe" +ISODATE:%%Y-%%m-%%d') do (
	if "%%a"=="ISODATE" set "ISO_DATE=%%b"
)
for /F "tokens=1,2,3,4 delims=:" %%a in ('"%~dp0\..\Prerequisites\GnuWin32\date.exe" +ISOTIME:%%T') do (
	if "%%a"=="ISOTIME" set "ISO_TIME=%%b:%%c:%%d"
)


REM ///////////////////////////////////////////////////////////////////////////
REM // Clean up temp files
REM ///////////////////////////////////////////////////////////////////////////

"%~dp0\..\Prerequisites\CEcho\cecho.exe" YELLOW "\n========[ CLEAN UP ]========\n"

for %%i in (bin,obj) do (
	del /Q /S /F "%~dp0\%%i\*.*"  2> NUL
)


REM ///////////////////////////////////////////////////////////////////////////
REM // Build the binaries
REM ///////////////////////////////////////////////////////////////////////////

"%~dp0\..\Prerequisites\CEcho\cecho.exe" YELLOW "\n========[ COMPILE ]========"

call "%MSVC_PATH%\vcvarsall.bat" x86
set "MSBuildUseNoSolutionCache=1"

for %%t in (Clean,Rebuild,Build) do (
	for %%c in (Release,Debug) do (
		for %%p in (x86,x64) do (
			MSBuild.exe /property:Platform=%%p /property:Configuration=%%c /target:%%t "%~dp0\MCat.sln"
			if not "!ERRORLEVEL!"=="0" goto BuildHasFailed
		)
	)
)


REM ///////////////////////////////////////////////////////////////////////////
REM // Generate Docs
REM ///////////////////////////////////////////////////////////////////////////

"%~dp0\..\Prerequisites\Pandoc\pandoc.exe" --from markdown_github+pandoc_title_block+header_attributes+implicit_figures --to html5 --toc -N --standalone -H "%~dp0\..\Prerequisites\Pandoc\css\github-pandoc.inc" "%~dp0\README.md" | "%JAVA_HOME%\bin\java.exe" -jar "%~dp0\..\Prerequisites\HTMLCompressor\bin\htmlcompressor-1.5.3.jar" --compress-css -o "%~dp0\README.html"
if not "%ERRORLEVEL%"=="0" goto BuildHasFailed


REM ///////////////////////////////////////////////////////////////////////////
REM // Generate output name
REM ///////////////////////////////////////////////////////////////////////////

mkdir "%~dp0\out" 2> NUL

set COUNTER=
set REVISON=

:GenerateOutfileNameNext
set "OUT_PATH=%~dp0\out\mcat.%ISO_DATE%%REVISON%"

set /a COUNTER=COUNTER+1
set REVISON=.update-%COUNTER%

if exist "%OUT_PATH%.zip" goto GenerateOutfileNameNext
if exist "%OUT_PATH%.source.tgz" goto GenerateOutfileNameNext


REM ///////////////////////////////////////////////////////////////////////////
REM // Build ZIP package
REM ///////////////////////////////////////////////////////////////////////////

"%~dp0\..\Prerequisites\CEcho\cecho.exe" YELLOW "\n========[ PACKAGING ]========\n"

set "PACK_PATH=%TMP%\~%RANDOM%%RANDOM%.tmp"
mkdir "%PACK_PATH%"
mkdir "%PACK_PATH%\img"
mkdir "%PACK_PATH%\img\mcat"

copy "%~dp0\COPYING.txt" "%PACK_PATH%\COPYING.txt"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

copy "%~dp0\README.html" "%PACK_PATH%\README.html"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

copy "%~dp0\img\mcat\mcat_logo.jpg" "%PACK_PATH%\img\mcat\mcat_logo.jpg"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

copy "%~dp0\bin\Win32\Release\mcat.exe" "%PACK_PATH%\mcat.exe"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

copy "%~dp0\bin\x64\Release\mcat.exe" "%PACK_PATH%\mcat.x64.exe"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

attrib +R "%PACK_PATH%\*"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

pushd "%PACK_PATH%"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

"%~dp0\..\Prerequisites\GnuWin32\zip.exe" -9 -r -z "%OUT_PATH%.zip" "*.*" < "%~dp0\COPYING.txt"
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

popd
if not "!ERRORLEVEL!"=="0" goto BuildHasFailed

"%GIT2_PATH%\git.exe" archive --verbose --format=tar.gz -9 > "%OUT_PATH%.source.tgz" HEAD
if not "%ERRORLEVEL%"=="0" goto BuildHasFailed

REM Clean up!
rmdir /Q /S "%PACK_PATH%"

REM ///////////////////////////////////////////////////////////////////////////
REM // Completed
REM ///////////////////////////////////////////////////////////////////////////

"%~dp0\..\Prerequisites\CEcho\cecho.exe" GREEN "\nBUILD COMPLETED.\n"
pause
goto:eof


REM ///////////////////////////////////////////////////////////////////////////
REM // Failed
REM ///////////////////////////////////////////////////////////////////////////

:BuildHasFailed
"%~dp0\..\Prerequisites\CEcho\cecho.exe" RED "\nBUILD HAS FAILED.\n"
pause
goto:eof
