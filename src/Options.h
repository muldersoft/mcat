///////////////////////////////////////////////////////////////////////////////
// MCat - Simple Port Probing Tool
// Copyright (C) 2017 LoRd_MuldeR <MuldeR2@GMX.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
// http://www.gnu.org/licenses/gpl-2.0.txt
///////////////////////////////////////////////////////////////////////////////

#ifndef INC_OPTIONS_H
#define INC_OPTIONS_H

typedef struct
{
	const wchar_t *name;
	char type;
}
options_spec_t;

typedef bool (options_handler_t)(const bool is_option, const size_t index, const void *const data, void *const user);
typedef void error_handler_t(void *const user, const wchar_t *const format, ...);
bool parse_options(const int argc, const wchar_t *const argv[], const char *const params, const options_spec_t *const spec, options_handler_t *const options_handler, error_handler_t *const error_handler, void *const user);

#endif //INC_OPTIONS_H
