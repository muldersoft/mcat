///////////////////////////////////////////////////////////////////////////////
// MCat - Simple Port Probing Tool
// Copyright (C) 2017 LoRd_MuldeR <MuldeR2@GMX.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
// http://www.gnu.org/licenses/gpl-2.0.txt
///////////////////////////////////////////////////////////////////////////////

//Internal
#include "Options.h"
#include "Utils.h"

//CRT
#include <stdexcept>
#include <cstdarg>
#include <io.h>
#include <fcntl.h>

//Win32
#include <winsock2.h>
#include <Ws2tcpip.h>

//Version
static const unsigned int versionMajor = 1U;
static const unsigned int versionMinor = 1U;

//Options
typedef struct
{
	const WCHAR *hostname;
	unsigned long port;
	unsigned long timeout;
	unsigned long retries;
	int flags;
	bool showhelp;
}
options_t;

//Protocol
static const int FLAG_PROTO_IPV4 = 1;
static const int FLAG_PROTO_IPV6 = 2;
static const int FLAG_FIRST_ONLY = 4;
static const int FLAG_QUIET_MODE = 8;

//Build date
static const char *const BUILD_DATE = __DATE__;

//Silent mode
#define IS_SILENT_MODE (options.flags & FLAG_QUIET_MODE)

///////////////////////////////////////////////////////////////////////////////
// Internal Functions
///////////////////////////////////////////////////////////////////////////////

static const WCHAR *trim_r(WCHAR *const str)
{
	size_t len = wcslen(str);
	while (len > 0)
	{
		len--;
		if (iswcntrl(str[len]) || iswspace(str[len]))
		{
			str[len] = L'\0';
			continue;
		}
		break;
	}
	return str;
}

static void printError(const int errorCode)
{
	WCHAR *message = NULL;
	if (FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&message, 0, NULL) > 0)
	{
		my_printf(L"Error #%d: %s\n\n", errorCode, trim_r(message));
		LocalFree(message);
	}
	else
	{
		my_printf(L"Error #%d: Unknown error.\n\n", errorCode);
	}
}

static const WCHAR *printAddr(const ADDRINFOW *const addr, WCHAR *const buffer, const size_t buffSize)
{
	DWORD buffSizeOut = static_cast<DWORD>(buffSize);
	const int result = WSAAddressToStringW(addr->ai_addr, static_cast<DWORD>(addr->ai_addrlen), NULL, buffer, &buffSizeOut);
	return (result == 0) ? buffer : L"N/A";
}

static void printLogo(void)
{
	static volatile bool completed = false;
	if(!completed)
	{
		my_printf(L"MCat v%u.%02u [%S]\n", versionMajor, versionMinor, BUILD_DATE);
		my_printf(L"Copyright(C) %S LoRd_MuldeR <MuldeR2@GMX.de>. Some Rights Reserved.\n\n", &BUILD_DATE[7]);
		completed = true;
	}
}

static void printHelp(void)
{
	my_puts(L"This program is free software : you can redistribute it and / or modify\n");
	my_puts(L"it under the terms of the GNU General Public License <http://www.gnu.org/>.\n");
	my_puts(L"Note that this program is distributed with ABSOLUTELY NO WARRANTY.\n");
	my_puts(L"\n");
	my_puts(L"Usage:\n");
	my_puts(L"   mcat.exe [OPTIONS] <target_name> [<port_number>]\n");
	my_puts(L"\n");
	my_puts(L"Options:\n");
	my_puts(L"   --timeout <timeout_msec>  Set connection timeout, in milliseconds\n");
	my_puts(L"   --retry <count>           Set number of reties, if connection fails\n");
	my_puts(L"   --ipv4                    Force IPv4, fails with IPv6-only hosts\n");
	my_puts(L"   --ipv6                    Force IPv6, fails with IPv4-only hosts\n");
	my_puts(L"   --first-only              Only try *first* IP address of the host\n");
	my_puts(L"   --silent                  Do *not* output any progress messages\n");
	my_puts(L"   --help                    Print help screen, then exit\n\n");
}

static bool handleArgValue(const bool is_option, const size_t index, const void *const data, void *const user)
{
	options_t *const options = reinterpret_cast<options_t*>(user);
	if(is_option)
	{
		switch(index)
		{
		case 0:
			options->showhelp = true;
			break;
		case 1:
			options->timeout = *reinterpret_cast<const unsigned long*>(data);
			break;
		case 2:
			options->retries = *reinterpret_cast<const unsigned long*>(data);
			break;
		case 3:
			options->flags |= FLAG_PROTO_IPV4;
			break;
		case 4:
			options->flags |= FLAG_PROTO_IPV6;
			break;
		case 5:
			options->flags |= FLAG_FIRST_ONLY;
			break;
		case 6:
			options->flags |= FLAG_QUIET_MODE;
			break;
		}
	}
	else
	{
		switch(index)
		{
		case 0:
			options->hostname = reinterpret_cast<const WCHAR*>(data);
			break;
		case 1:
			options->port = *reinterpret_cast<const unsigned long*>(data);
			break;
		}
	}

	return true;
}

static void handleArgError(void *const user, const WCHAR *const format, ...)
{
	printLogo();
	my_vprintf(format);
}

static bool parseArgs(options_t &options, const int &argc, const wchar_t *const argv[])
{
	memset(&options, 0, sizeof(options_t));
	options.port = 80U;

	static const options_spec_t OPTION_SPEC[] =
	{
		{ L"help",       '0' }, //0
		{ L"timeout",    'u' }, //1
		{ L"retry",      'u' }, //2
		{ L"ipv4",       '0' }, //3
		{ L"ipv6",       '0' }, //4
		{ L"first-only", '0' }, //5
		{ L"silent",     '0' }, //6
		{ NULL, NULL }
	};

	if(!parse_options(argc, argv, "su", OPTION_SPEC, handleArgValue, handleArgError, &options))
	{
		return false;
	}

	if((options.flags & FLAG_PROTO_IPV4) && (options.flags & FLAG_PROTO_IPV6))
	{
		printLogo();
		my_puts(L"The options \"--ipv4\" and \"--ipv6\" are mutually exclusive!\n\n");
		return false;
	}

	if(!options.hostname)
	{
		printLogo();
		my_puts(L"Target name not specified! Type \"mcat.exe --help\" for details.\n\n");
		return false;
	}

	if((options.port == 0) || (options.port > USHRT_MAX))
	{
		printLogo();
		my_printf(L"The specified port %u is invalid. Must be in 1 to %u range!\n\n", options.port, USHRT_MAX);
		return false;
	}

	return true;
}

static bool resolveAddr(ADDRINFOW **const addrOut, const WCHAR *const name, const unsigned long port, const options_t &options)
{
	*addrOut = NULL;

	for(int resolve = 0; resolve < 2; ++resolve)
	{

		ADDRINFOW hints;
		memset(&hints, 0, sizeof(addrinfo));
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_socktype = SOCK_STREAM;
		
		if(options.flags & FLAG_PROTO_IPV4)
		{
			hints.ai_family = AF_INET;
		}
		else if(options.flags & FLAG_PROTO_IPV6)
		{
			hints.ai_family = AF_INET6;
		}

		if(resolve)
		{
			if(!IS_SILENT_MODE)
			{
				my_printf(L"Resolving host %s...\n", name);
			}
		}
		else
		{
			hints.ai_flags |= AI_NUMERICHOST;
		}

		WCHAR service[32];
		_snwprintf_s(service, 32, _TRUNCATE, L"%lu", port);

		const int result = GetAddrInfoW(name, service, &hints, addrOut);
		if (result != 0)
		{
			if(resolve)
			{
				if(!IS_SILENT_MODE)
				{
					my_puts(L"Resolution failed!\n\n");
					printError(result);
				}
				break;
			}
		}
		else
		{
			if(((*addrOut)->ai_family == AF_INET) || ((*addrOut)->ai_family == AF_INET6))
			{
				if(resolve && (!IS_SILENT_MODE))
				{
					my_puts(L"Succeeded.\n\n");
				}
				return true;
			}
			else
			{
				FreeAddrInfoW(*addrOut);
				*addrOut = NULL;
			}
		}
	}

	return false;
}

static const bool waitSocket(const SOCKET sock, const unsigned int timeout, int &error)
{
	fd_set fdSet;
	FD_ZERO(&fdSet);
	FD_SET(sock, &fdSet);

	TIMEVAL timeval;
	timeval.tv_sec = timeout / 1000U;
	timeval.tv_usec = (timeout % 1000U) * 1000U;

	const int result = select(0, NULL, &fdSet, NULL, &timeval);
	if (result < 1)
	{
		if (result != 0)
		{
			error = WSAGetLastError();
		}
		else
		{
			shutdown(sock, SD_BOTH);
			error = WSAETIMEDOUT;
		}
		return false;
	}
	else
	{
		error = 0;
		return true;
	}
}

static const bool connectAddr(const ADDRINFOW *const addr, const options_t &options)
{
	bool success = false;

	if(!IS_SILENT_MODE)
	{
		WCHAR addrStr[128];
		my_printf(L"Connecting to %s...\n", printAddr(addr, addrStr, 128));
	}

	const SOCKET sock = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
	if (sock == INVALID_SOCKET)
	{
		if(!IS_SILENT_MODE)
		{
			const int error = WSAGetLastError();
			my_puts(L"Failed to create socket!\n\n");
			printError(error);
		}
		return false;
	}
	
	if (options.timeout > 0)
	{
		u_long imode = 1;
		if (ioctlsocket(sock, FIONBIO, &imode) != NO_ERROR)
		{
			if(!IS_SILENT_MODE)
			{
				my_puts(L"ioctlsocket() failed!\n\n");
			}
			return false;
		}
	}

	int error = -1;
	if (connect(sock, addr->ai_addr, static_cast<int>(addr->ai_addrlen)) == 0)
	{
		success = true;
	}
	else
	{
		if ((error = WSAGetLastError()) == WSAEWOULDBLOCK)
		{
			success = waitSocket(sock, options.timeout, error);
		}
	}

	if(!IS_SILENT_MODE)
	{
		if(success)
		{
			my_puts(L"Succeeded.\n\n");
		}
		else
		{
			my_printf(L"Connection %s!\n\n", (error == WSAETIMEDOUT) ? L"timeout" : L"failed");
			printError(error);
		}
	}

	closesocket(sock);
	return success;
}

///////////////////////////////////////////////////////////////////////////////
// Main
///////////////////////////////////////////////////////////////////////////////

static const int EXIT_LOOKUP_FAILED     = 2;
static const int EXIT_CONNECTION_FAILED = 3;
static const int EXIT_SYSTEM_ERROR      = 4;
static const int EXIT_WAS_INTERRUPTED   = 5;

static int mcat_main(const int &argc, const wchar_t *const argv[])
{
	options_t options;
	if(!parseArgs(options, argc, argv))
	{
		return EXIT_FAILURE; /*bad arguments*/
	}

	if (options.showhelp)
	{
		printLogo();
		printHelp();
		return EXIT_SUCCESS;
	}

	if(!IS_SILENT_MODE)
	{
		printLogo();
	}

	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		my_puts(L"System error: WSAStartup() function failed!\n\n");
		return EXIT_SYSTEM_ERROR;
	}

	ADDRINFOW *addr = NULL;
	if(!resolveAddr(&addr, options.hostname, options.port, options))
	{
		WSACleanup();
		return EXIT_LOOKUP_FAILED;
	}
	
	bool success = false;
	for(unsigned long attempt = 0; attempt <= options.retries; ++attempt)
	{
		if((attempt > 0) && (!IS_SILENT_MODE))
		{
			my_printf(L"Retrying! [retry %lu of %lu]\n\n", attempt, options.retries);
		}
		for (ADDRINFOW *current = addr; current != NULL; current = current->ai_next)
		{
			if (success = connectAddr(current, options))
			{
				goto completed; /*succeeded*/
			}
			else if(options.flags & FLAG_FIRST_ONLY)
			{
				break; /*fail after first*/
			}
		}
	}

completed:
	FreeAddrInfoW(addr);
	WSACleanup();
	return success ? EXIT_SUCCESS : EXIT_CONNECTION_FAILED;
}

///////////////////////////////////////////////////////////////////////////////
// Error Handling
///////////////////////////////////////////////////////////////////////////////

static void my_invalid_param_handler(const wchar_t* exp, const wchar_t* fun, const wchar_t* fil, unsigned int, uintptr_t)
{
	my_puts(L"\n\nFATAL ERROR: Invalid parameter handler invoked!\n");
	TerminateProcess(GetCurrentProcess(), 666);
}

static LONG WINAPI my_exception_handler(struct _EXCEPTION_POINTERS *ExceptionInfo)
{
	my_puts(L"\n\nFATAL ERROR: Unhandeled exception handler invoked!\n");
	TerminateProcess(GetCurrentProcess(), 666);
	return LONG_MAX;
}

static BOOL WINAPI my_console_ctrl_handler(DWORD dwCtrlType)
{
	switch(dwCtrlType)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
		my_puts(L"\n\nSIGINT: Interrupted, program will terminate now!\n");
		ExitProcess(EXIT_WAS_INTERRUPTED);
		return TRUE;
	default:
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Entry Point
///////////////////////////////////////////////////////////////////////////////

static int wmain_ex(const int &argc, const wchar_t *const argv[])
{
	int ret = -1;
	try
	{
		ret = mcat_main(argc, argv);
	}
	catch (std::exception& err)
	{
		my_printf(L"\n\nUNHANDELED EXCEPTION: %S\n", err.what());
		ret = 666;
	}
	catch (...)
	{
		my_puts(L"\n\nUNHANDELED UNKNOWN C++ EXCEPTION !!!\n");
		ret = 666;
	}
	return ret;
}

int wmain(int argc, wchar_t* argv[])
{
	int ret = -1;
#ifdef NDEBUG
#ifdef _DEBUG
#error NDEBUG and _DEBUG are mutually exclusive!
#endif
	__try
	{
		SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOOPENFILEERRORBOX);
		SetUnhandledExceptionFilter(my_exception_handler);
		SetDllDirectoryW(L""); /*don'tload DLL from "current" directory*/
		SetConsoleCtrlHandler(my_console_ctrl_handler, TRUE);
		_set_invalid_parameter_handler(my_invalid_param_handler);
		_setmode(_fileno(stderr), _O_U8TEXT);
		ret = wmain_ex(argc, argv);
	}
	__except (1)
	{
		my_puts(L"\n\nUNHANDELED WIN32 EXCEPTION ERROR !!!\n\n");
		_exit(666);
	}
#else
	_setmode(_fileno(stderr), _O_U8TEXT);
	ret = mcat_main(argc, argv);
#endif

	return ret;
}