///////////////////////////////////////////////////////////////////////////////
// MCat - Simple Port Probing Tool
// Copyright (C) 2017 LoRd_MuldeR <MuldeR2@GMX.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
// http://www.gnu.org/licenses/gpl-2.0.txt
///////////////////////////////////////////////////////////////////////////////

#ifndef INC_UTILS_H
#define INC_UTILS_H

#include <cstdio>

#define my_printf(FMT, ...) do \
{ \
	fwprintf(stderr, (FMT), __VA_ARGS__); \
	fflush(stderr); \
} \
while(0)

#define my_puts(STR) do \
{ \
	fputws((STR), stderr); \
	fflush(stderr); \
} \
while(0)

#define my_vprintf(FMT) do \
{ \
	va_list args; \
	va_start (args, (FMT)); \
	vfwprintf (stderr, (FMT), args); \
	va_end (args); \
} \
while(0)

#endif //INC_UTILS_H
