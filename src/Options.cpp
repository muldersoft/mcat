///////////////////////////////////////////////////////////////////////////////
// MCat - Simple Port Probing Tool
// Copyright (C) 2017 LoRd_MuldeR <MuldeR2@GMX.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
// http://www.gnu.org/licenses/gpl-2.0.txt
///////////////////////////////////////////////////////////////////////////////

#include "Options.h"
#include <cstdio>
#include <cstring>

#define NEXT_ARG_VALUE() do \
{ \
	if(++i >= argc) \
	{ \
		my_print("Argument for command-line option \"%S\" is missing!\n\n", argv[i-1]); \
		return false; \
	} \
} \
while(0)

static bool parse_value(const wchar_t *const argv, const char type, options_handler_t *const options_handler, error_handler_t *const error_handler, const bool is_option, const size_t index, void *const user)
{
	switch(type)
	{
	case '0':
		return options_handler(is_option, index, NULL, user);
	case 's':
		return options_handler(is_option, index, argv, user);
	case 'i':
		if(argv)
		{
			long value;
			if(swscanf_s(argv, L"%ld", &value) >= 1)
			{
				return options_handler(is_option, index, &value, user);
			}
		}
		error_handler(user, L"Invalid argument value \"%s\" specified!\n\n", argv);
		return false;
	case 'u':
		if(argv)
		{
			unsigned long value;
			if(swscanf_s(argv, L"%lu", &value) >= 1)
			{
				return options_handler(is_option, index, &value, user);
			}
		}
		error_handler(user, L"Invalid argument value \"%s\" specified!\n\n", argv);
		return false;
	default:
		error_handler(user, L"INTERNAL ERROR: Option type '%C' is unsupported!\n\n", type);
		return false;
	}
}

bool parse_options(const int argc, const wchar_t *const argv[], const char *const params, const options_spec_t *const spec, options_handler_t *const options_handler, error_handler_t *const error_handler, void *const user)
{
	bool parse_options = true;
	size_t counter = 0;

	for(int i = 1; i < argc; i++)
	{
		if(parse_options && (!_wcsnicmp(argv[i], L"--", 2)))
		{
			bool okay = false;
			const wchar_t *const optname = argv[i] + 2;
			if(*optname == L'\0')
			{
				okay = true;
				parse_options = false; /*stop option parsing*/
			}
			else 
			{
				for(size_t index = 0; spec[index].name; ++index)
				{
					if(!_wcsicmp(optname, spec[index].name))
					{
						okay = true;
						if(spec[index].type != '0')
						{
							if(++i >= argc)
							{
								error_handler(user, L"Argument for command-line option \"--%s\" is missing!\n\n", optname);
								return false;
							}
						}
						if(!parse_value(argv[i], spec[index].type, options_handler, error_handler, true, index, user))
						{
							return false;
						}
						break;
					}
				}
			}
			if(!okay)
			{
				error_handler(user, L"Unknown command-line option \"--%s\" encountered!\n\n", optname);
				return false;
			}
			continue;
		}
		else
		{
			parse_options = false;
			if(const char param_type = params[counter])
			{
				if(!parse_value(argv[i], param_type, options_handler, error_handler, false, counter++, user))
				{
					return false;
				}
			}
			else
			{
				error_handler(user, L"Excess command-line parameter \"%s\" encountered!\n\n", argv[i]);
				return false;
			}
		}
	}

	return true;
}
