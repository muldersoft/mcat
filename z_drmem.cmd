@echo off
set "DRMEMORY_HOME=C:\Program Files (x86)\Dr. Memory"

if not exist "%DRMEMORY_HOME%\bin\drmemory.exe" (
	echo Dr. Memory could not be found, please make sure DRMEM_PATH is set correctly!
	pause && goto:eof
)

set "PATH=%DRMEMORY_HOME%\bin;%PATH%"
cd /d "%~dp0\bin\Win32\Debug"

"%DRMEMORY_HOME%\bin\drmemory.exe" "%cd%\mcat.exe" "www.heise.de"
"%DRMEMORY_HOME%\bin\drmemory.exe" "%cd%\mcat.exe" --ipv4 "www.heise.de"
"%DRMEMORY_HOME%\bin\drmemory.exe" "%cd%\mcat.exe" --ipv6 "www.heise.de"
"%DRMEMORY_HOME%\bin\drmemory.exe" "%cd%\mcat.exe" "[2a02:2e0:3fe:1001:7777:772e:2:85]"

pause
